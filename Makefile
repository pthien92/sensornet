# /*
#  * Copyright (c) 2015 Thanh Thien Pham and The University of Adelaide
#  * All rights reserved.
#  *
#  * Redistribution and use in source and binary forms, with or without
#  * modification, are permitted provided that the following conditions
#  * are met:
#  *
#  * - Redistributions of source code must retain the above copyright
#  *   notice, this list of conditions and the following disclaimer.
#  *
#  * - Redistributions in binary form must reproduce the above copyright
#  *   notice, this list of conditions and the following disclaimer in the
#  *   documentation and/or other materials provided with the
#  *   distribution.
#  *
#  * - Neither the name of the copyright holders nor the names of
#  *   its contributors may be used to endorse or promote products derived
#  *   from this software without specific prior written permission.
#  *
#  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
#  * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
#  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#  * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
#  * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
#  * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
#  * OF THE POSSIBILITY OF SUCH DAMAGE.
#  */

#  /*
#   * This is the makefile to be used with newest version of TinyOS
#   * If TinyOS is built from source then this makefile is used because
#   * TinyOS 2.1.2 uses a new version of makefike. 
#   * Expecting Mac OS X and Windows users use this makefile.
#   *
#   * Author: Thanh Thien Pham <a1615326@student.adelaide.edu.au>
#   * Date: 15/01/2015
#   */

COMPONENT=SensorAppC
CFLAGS += -I$(TINYOS_OS_DIR)/lib/net/ -I$(TINYOS_OS_DIR)/lib/net/ctp -I$(TINYOS_OS_DIR)/lib/net/4bitle -I.
CFLAGS += -I$(TINYOS_OS_DIR)/sensorboards/mts300/ 
CFLAGS += -DTOSH_DATA_LENGTH=30 -DATM128_I2C_EXTERNAL_PULLDOWN=1
SENSORBOARD=mts300
TOSMAKE_PRE_EXE_DEPS = SensorNetMsg.class

SensorNetMsg.java: SensorNet.h
	nescc-mig -I$(TINYOS_OS_DIR)/types -java-classname=SensorNetMsg java SensorNet.h sensornet_msg -o $@

SensorNetMsg.class: SensorNetMsg.java
	javac SensorNetMsg.java

TINYOS_ROOT_DIR?=../..
include $(TINYOS_ROOT_DIR)/Makefile.include