/*
 * Copyright (c) 2015 Thanh Thien Pham and The University of Adelaide
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * - Neither the name of the copyright holders nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

 /*
  * This is the header file for SensorNet application. 
  *
  * Author: Thanh Thien Pham <a1615326@student.adelaide.edu.au>
  * Date: 15/01/2015
  */
#ifndef SENSORNET_H
#define SENSORNET_H
#include "AM.h"
#define CC2420_DEF_CHANNEL 26 /* 2480 MHz */
#ifndef MTS300CB /* Sensor hardware identity */
	#define MTS300CB
#endif

enum {
	/* Default sampling period */
	DEFAULT_INTERVAL = 1024 /* milli sec */,
	AM_SENSORNET_MSG = 0x92,
};

typedef nx_struct sensornet_msg {
	nx_uint16_t interval; /* sampling period */
	nx_uint16_t origin;   /* Mote id of sending mote */
	nx_uint16_t count;    /* The number of readings/periods */
	//nx_uint32_t timestamp; /* Keep track of the time of transmitting packet */
	nx_uint16_t sound_presence; /* store the variance in ambient soundness */
	nx_uint16_t temp_reading;   /* The raw temperature sensor value */
	nx_uint16_t light_reading; /* The raw light sensor value */
	nx_uint16_t etx; /* Value for routing algorithm */
	nx_uint16_t link_route_value;
	nx_am_addr_t link_route_addr;
	} sensornet_msg_t;

#endif
