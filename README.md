Copyright ® 2015 Thanh Thien Pham and The University of Adelaide
BSD licence

MESH WIRELESS SENSOR NETWORK - CROSSBOW MICAZ platform

1. Install the tinyOS using the Installation document in /Documentation
2. To install the whole application folder:
  Copy the whole folder into tinyos-main/apps/
3. Makefile is used for Mac OS X and Widows/Cygwin, while Makefile.ubuntu (renamed when used) is for Ubuntu
There includes 2 packetListener tools: one for MATLAB (packetListener.m) and the other for Python (packetListener.py).

Documentation and reports can be found in /Documentation
