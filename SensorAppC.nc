/*
 * Copyright (c) 2015 Thanh Thien Pham and The University of Adelaide
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * - Neither the name of the copyright holders nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

 /*
  * This is the cofiguration file for SensorNet application. 
  * The wiring steps happen here
  *
  * Author: Thanh Thien Pham <a1615326@student.adelaide.edu.au>
  * Date: 15/01/2015
  */
#include "SensorNet.h"
configuration SensorAppC {}
implementation {
	/* Basic components wiring */
	components MainC, SensorNetC, LedsC, PhotoTempDeviceC, RandomC, new MicC();
	components new TimerMilliC() as mainTimer;// new TimerMilliC() as microphoneTimer;
	SensorNetC.Boot -> MainC;
	SensorNetC.Timer -> mainTimer;
	//SensorNetC.MicTimer -> microphoneTimer;
	SensorNetC.TempControl -> PhotoTempDeviceC.TempResource[1];
	SensorNetC.LightControl -> PhotoTempDeviceC.PhotoResource[2];
	SensorNetC.ReadTemp -> PhotoTempDeviceC.ReadTemp[1];
	SensorNetC.ReadPhoto -> PhotoTempDeviceC.ReadPhoto[2];
	SensorNetC.ReadMic -> MicC;
	SensorNetC.MicSetting -> MicC;
	SensorNetC.Leds -> LedsC;
	SensorNetC.Random -> RandomC;

	/* Network layers */
	components CollectionC as Collector; /* Collection layer */
	components ActiveMessageC;  /* AM layer */
	components new CollectionSenderC(AM_SENSORNET_MSG); /* Send multihop (mesh) RF */
	components SerialActiveMessageC; /* Serial messaging */
	components new SerialAMSenderC(AM_SENSORNET_MSG); /* Send to the serial */
	components CtpP as Ctp;

	SensorNetC.RadioControl -> ActiveMessageC;
	SensorNetC.SerialControl -> SerialActiveMessageC;
	SensorNetC.RoutingControl -> Collector;
	//SensorNetC.TimeStamp -> ActiveMessageC.PacketTimeStampMilli;
	SensorNetC.Send -> CollectionSenderC;
	SensorNetC.SerialSend -> SerialAMSenderC.AMSend;
	SensorNetC.Snoop -> Collector.Snoop[AM_SENSORNET_MSG];
	SensorNetC.Receive -> Collector.Receive[AM_SENSORNET_MSG];
	SensorNetC.RootControl -> Collector;
	SensorNetC.CtpInfo -> Ctp;
	SensorNetC.LinkEstimator -> Ctp;

}