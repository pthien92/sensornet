# /*
#  * Copyright (c) 2015 Thanh Thien Pham and The University of Adelaide
#  * All rights reserved.
#  *
#  * Redistribution and use in source and binary forms, with or without
#  * modification, are permitted provided that the following conditions
#  * are met:
#  *
#  * - Redistributions of source code must retain the above copyright
#  *   notice, this list of conditions and the following disclaimer.
#  *
#  * - Redistributions in binary form must reproduce the above copyright
#  *   notice, this list of conditions and the following disclaimer in the
#  *   documentation and/or other materials provided with the
#  *   distribution.
#  *
#  * - Neither the name of the copyright holders nor the names of
#  *   its contributors may be used to endorse or promote products derived
#  *   from this software without specific prior written permission.
#  *
#  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#  * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
#  * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
#  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#  * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
#  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
#  * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
#  * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
#  * OF THE POSSIBILITY OF SUCH DAMAGE.
#  */

#  /*
#   * Packet Sniffer and Logging tool to be used with TinyOS-powered sensor motes
#	* Display packet data on screen and save data into a json file format
#	* Tested in Mac OS X 10.10
#   * SERIAL_PORT is subject to change according to OS
#   * Author: Thanh Thien Pham <a1615326@student.adelaide.edu.au>
#   * Date: 30/01/2015
#   */


SERIAL_PORT = "COM4"
BAUD_RATE = 57600

import serial
import json
import string
import io
import time
import signal

#redefine the interrupt handler when Ctrl-C is pressed
def signal_handler(signum, frame):
	print("Warning: custom interrupt handler called.")

# Set up the serial
sp = serial.Serial(SERIAL_PORT,BAUD_RATE, timeout=10)
sp.flush()

# Prepare the log file
filename = "python_sensor_data_" + time.strftime("%Y-%m-%d-%H:%M:%S") + ".json"
f = open(filename,'w')
f.write('[\n')
print ("Sensor Data Logging - saving to" + filename)
print ("Press Ctrl-C to terminate")

# main loop
try:
	while(1):   
		# Read 31 bytes from serial                
	    response = sp.read(31).encode('hex')
	    # Check if the packet is complete, ie. the terminator is '7e'
	    if (response[60] == '7' and response[61] == 'e'):
		    print ""
		    print("Raw packet:  " + response)

		    #get the payload and convert to int
		    data = int(response[20:44],16)
		    light = data & 0xFFFF
		    temp = (data >> 16) & 0xFFFF
		    sound = (data >> 32) & 0xFFFF
		    sound = (data >> 32) & 0xFFFF
		    time = (data >> 48) & 0xFFFF
		    origin = (data >> 64) & 0xFFFF
		    interval = data >> 80

		    #Print out the information
		    print "---------------------------------------------------------------------------------------------------------"
		    print("Interval\t\tOrigin\t\tTime\t\tSound\t\tTemperature\t\tLight")
		    print("%s/%d\t\t%s/%d\t\t%s/%d\t%s/%d\t%s/%d\t\t%s/%d" % (response[20:24],interval, response[24:28],origin,
		     response[28:32], time, response[32:36], sound, response[36:40], temp, response[40:44], light))
		    print "---------------------------------------------------------------------------------------------------------"

		    #Dump the data into json file using json 
		    json.dump({"origin":origin,"interval":interval,"time":time,"sound":sound,"temp":temp,"light":light},f)
		    f.write(',\n')
	    else:
	    	#Packet is slipped, reset the serial connection
			sp.close()
			sp.open()
#Perform clean up operation when program terminates
except KeyboardInterrupt:
	print("\nWarning: Interrupt received, cleaning up ...")
	f.seek(-2,2)
	f.truncate()
	f.write('\n]')
	f.close()
	sp.close()
	print("Done. Exit successfully! - File saved as " + filename)
    
    	
    
