% /*
%  * Copyright (c) 2015 Thanh Thien Pham and The University of Adelaide
%  * All rights reserved.
%  *
%  * Redistribution and use in source and binary forms, with or without
%  * modification, are permitted provided that the following conditions
%  * are met:
%  *
%  * - Redistributions of source code must retain the above copyright
%  *   notice, this list of conditions and the following disclaimer.
%  *
%  * - Redistributions in binary form must reproduce the above copyright
%  *   notice, this list of conditions and the following disclaimer in the
%  *   documentation and/or other materials provided with the
%  *   distribution.
%  *
%  * - Neither the name of the copyright holders nor the names of
%  *   its contributors may be used to endorse or promote products derived
%  *   from this software without specific prior written permission.
%  *
%  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
%  * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
%  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
%  * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
%  * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
%  * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
%  * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
%  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
%  * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
%  * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
%  * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
%  * OF THE POSSIBILITY OF SUCH DAMAGE.
%  */

%  /*
%   * 
%   * 
%   *
%   * Author: Thanh Thien Pham <a1615326@student.adelaide.edu.au>
%   * Date: 30/01/2015
%   
%
% Description: This tool can capture the packet from serial port
% Display the intepreted data on screen 
% Record the data to file in JSON format

%Initialization

clear all
format shortg
fclose('all');

%Create serial connection to ie. COM4, COM4 is subject to change
s = serial('COM4');
set(s,'BaudRate',57600);
set(s,'InputBufferSize',50000);


%Check whether the serial is opened or not
if (isempty(instrfind('Status','open')))
    %Serial port is not opened yet
    fopen(s);
else
    % Serial port is opened, try to close it then reopen
    fclose(instrfind('Status','open'));
    fopen(s);
end



fprintf('******************************\nInit serial port successfully\n******************************\n\n')
fprintf('******NOTICE******\nPress Ctrl-C to exit loop\nPress Clean Up button to finalise record data\n\n')




% Create a time-tracking fashion for record data file name ie.
% sensor_data_2015-1-27-16-0-23
date = clock;
current = sprintf('%d-%d-%d-%d-%d-%d',date(1), date(2), date(3), date(4), date(5),round(date(6)));
name = strcat('sensor_data_',current,'.json');
recordFile = fopen(name,'w');
fprintf(recordFile,'%s\n','{"UofA WSN": { "motes": [ ');




% Create a simple clean up button for control the logging process
Frame = figure('Position',[200, 200, 200, 200]);
H = uicontrol('Style', 'PushButton', ...
                    'String', 'Clean Up', ...
                    'Callback', {@onCleanUp,recordFile,Frame,name});
figure(Frame);





% Loop infinitely to get packet data from serial port - Ctrl-C to exit
while (1)
    % Read data per byte - 31 bytes in total
    % s.BytesAvailable is the remaining byte we missed
    data = fread(s,31+s.BytesAvailable,'uint8');
    data = data';
    
    
    
    % There is a total of 31 bytes of data per packet, it is usual to get
    % insufficent data ie. 30 packet so we need to get a complete packet in
    % order to extract correct data
    if (data(31) == 126) %packet terminator = 7E = 126, else drop bad packet
        fprintf('Raw packet: ');
        %Print data in hex format, 2 digit width, 0-heading is displayed
        for i=1:31
           fprintf('%02X%02X ',data(i));
        end
        
        % Extract payload data from packet, each variable presents an
        % uint16_t
        interval    =       2^8*data(11) + data(12);
        origin      =       2^8*data(13) + data(14);
        time        =       2^8*data(15) + data(16);
        sound       =       2^8*data(17) + data(18);
        temp        =       2^8*data(19) + data(20);
        light       =       2^8*data(21) + data(22);
        
        %Print out the data to screen
        fprintf('\n--------------------------------------------------------------------------------\n');
        fprintf('Interval\t\tOrigin\t\tTime\t\t\tSound\t\tTemperature\t\tLight\n')
        fprintf('%02X%02X/%d\t\t%02X%02X/%d\t\t%02X%02X/%d\t\t%02X%02X/%d\t%02X%02X/%d\t\t%02X%02X/%d',... 
            data(11),data(12), interval,...
            data(13),data(14), origin,...
            data(15),data(16), time,...
            data(17),data(18), sound,...
            data(19),data(20), temp,...
            data(21), data(22), light)
        fprintf('\n--------------------------------------------------------------------------------\n\n')
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Write to file sensor_data_....          %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        fprintf(recordFile,'{ "origin":%d , "interval":%d , "time":%d , "sound":%d , "temperature":%d , "light":%d },\n',...
            origin, interval, time, sound, temp, light);    
    end
    
end



    