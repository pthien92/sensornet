/*
 * Copyright (c) 2015 Thanh Thien Pham and The University of Adelaide
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * - Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the
 *   distribution.
 *
 * - Neither the name of the copyright holders nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

 /*
  * This is the implementation of SensorNetC component. 
  *
  * Author: Thanh Thien Pham <a1615326@student.adelaide.edu.au>
  * Date: 15/01/2015
  */


 #include "SensorNet.h"
 #include <avr/../math.h> /* This is to make sure the AVR math.h library is included */
 module SensorNetC {
 	uses {
 		/* Initialization of interfaces */

 		interface Boot;
 		interface SplitControl as RadioControl;
 		interface SplitControl as SerialControl;
 		interface StdControl as RoutingControl;
 		interface Resource as TempControl;
 		interface Resource as LightControl;

 		/* Interface for communication, multihop and serial */

 		interface Send;
 		interface Receive as Snoop;
 		interface Receive;
 		interface AMSend as SerialSend;
 		interface CollectionPacket;
 		interface RootControl;

 		/* Timer and ADC interfaces, Link Quanlity */

 		interface Timer<TMilli>;
 		//interface Timer<TMilli> as MicTimer;
 		interface Read<uint16_t> as ReadTemp; /* Read temp sensor */
 		interface Read<uint16_t> as ReadPhoto; /* Read light sensor */
 		interface Read<uint16_t> as ReadMic;
 		interface MicSetting;
 		interface Leds;
 		interface CtpInfo;
 		interface LinkEstimator;
 		interface Random;
 		//interface PacketTimeStamp<TMilli, uint32_t> as TimeStamp;
 	}
 }

 implementation {

 	/* Functions and tasks */
 	task void UARTSendTask(); /* sending packets to host via uart if node is root */
 	task void TempConvertionTask(); /* Convert temperature reading to Celsius Degree */
 	task void readMicTask(); /* Read microphone value */
 	static void startTimer();
 	static void init_error();
 	static void LED_SignalError();
 	static void LED_SignalSent();
 	static void LED_SignalReceived();
    /* Variables */
 	uint8_t UARTLength;
 	message_t sendBuffer;
 	message_t UARTBuffer;
 	bool sendBusy=FALSE;
 	bool uartBusy=FALSE;
 	/* Local state */
 	sensornet_msg_t local;
 	/* Cheap Time Synchronization */
 	//bool time_sync;

 	/* Init stage finished, call back events */

 	event void Boot.booted() {
 		local.interval = DEFAULT_INTERVAL;
 		local.origin = TOS_NODE_ID;
 		local.temp_reading = 0;
 		local.light_reading = 0;
 		local.sound_presence = 0;
 		//local.timestamp = 0;

 		/* Time to init other modules */
 		if (call RadioControl.start() != SUCCESS)
 			init_error();
 		if (call RoutingControl.start() != SUCCESS)
 			init_error();
 		if (call MicSetting.startMic() != SUCCESS)
 			init_error();
 			/* Enable interrupt for microphone 
 		 	 * If there is an interrupt, it signals toneDectected() event */
 		    else {
 		     	call MicSetting.enable();
 		    }
 	}

 	/* Call-back events for radio and routing */

 	event void RadioControl.startDone(error_t error) {
 		if (error != SUCCESS)
 			init_error();
 		/* Check the size of the message against the maximum */
 		if (sizeof(local) > call Send.maxPayloadLength())
 			init_error();
 		/* When we reached this stage, it is time to start serial */
 		if (call SerialControl.start() != SUCCESS)
 			init_error();
 	}

 	/* Call-back event for SerialControl */
 	event void SerialControl.startDone(error_t error) {
 		if (error != SUCCESS)
 			init_error();
 		/* Set this node as root of collection via serial */
 		if (local.origin % 500 == 0)
 			call RootControl.setRoot();

 		/* Finished all of initialization, start timer now */
 		startTimer();
 	}

 	static void startTimer() {
 		/* Setup the timer with the period = pre-defined interval */
 		call Timer.startPeriodic(local.interval);
 	}
 	/* There are nothing to do with these events */
 	event void RadioControl.stopDone(error_t error) { }
 	event void SerialControl.stopDone(error_t error) { }

 	/* Processing the packets received, if this is a root,
 	 * forward the message via UART to host PC
 	 */
 	 event message_t* Receive.receive(message_t* msg, void* payload, uint8_t len) {
 	 	if (uartBusy == FALSE) {
 	 		sensornet_msg_t* input = (sensornet_msg_t*) payload;
 	 		sensornet_msg_t* output = (sensornet_msg_t*) call SerialSend.getPayload(&UARTBuffer, sizeof(sensornet_msg_t));
 	 		if (output == NULL) { /* No data available for collecting */
 	 			return msg;
 	 		} else {
 	 			/* There is something to send */
 	 			memcpy(output, input, sizeof(sensornet_msg_t));
 	 		}
 	 		uartBusy = TRUE;
 	 		UARTLength = sizeof(sensornet_msg_t);
 	 		post UARTSendTask();
 	 	}
 	 	return msg;
 	 }

 	 /* Send the AM-format packet via UART protocol to host PC */
 	 task void UARTSendTask() {
 	 	
 	 		if (call SerialSend.send(0xffff,&UARTBuffer, UARTLength) != SUCCESS) {
 	 			uartBusy = FALSE;
 	 		}
 	 	
 	 }

 	 /* Overhearing the traffic */

 	 event message_t* Snoop.receive(message_t* msg, void* payload, uint8_t len) {
 	 	//sensornet_msg_t* overhear_msg = (sensornet_msg_t*) payload;
 	 	LED_SignalReceived();
 	 	/* Time sync required if larger count is detected 
 	 	if (overhear_msg->count > local.count) {
 	 		local.count = overhear_msg->count;
 	 		time_sync = TRUE;
 	 	} */

 	 	return msg;
 	 }

 	 /* At each sample period:
 	  - if local sample buffer is full, send accumulated samples
 	  - read next sample
 	 */
 	 event void Timer.fired() {
 	 	if (!sendBusy) {
 	 		sensornet_msg_t* output = (sensornet_msg_t*) call Send.getPayload(&sendBuffer, sizeof(sensornet_msg_t));
 	 		if (output == NULL) {
 	 			LED_SignalError();
 	 			return;
 	 		}
 	 		memcpy(output,&local, sizeof(local));
 	 		if (call Send.send(&sendBuffer, sizeof(local)) == SUCCESS)
 	 			sendBusy = TRUE;
 	 		else
 	 			LED_SignalError();
 	 	}
 	 	//local.count=call Timer.getNow();
 	 	local.count++;
 	 	/* Clean-up job of cheap time sync job 
 	 	if (!time_sync)
 	 		local.count++;
 	 	time_sync = FALSE;
 	 	call Timer.stop();
 	 	call Timer.startPeriodic(local.interval); */
 	 	/* Acquired access to temperature resource */
 	 	call TempControl.request();
 	 }
 	/* If we are permitted to use the temp resource */
 	event void TempControl.granted() {
 		/* Read temperature value */

 		if (call ReadTemp.read() != SUCCESS)
 			LED_SignalError();
 	}

 	/* Call-back event for sending message */
 	event void Send.sendDone(message_t* msg, error_t error) {
 		if (error == SUCCESS) {
 			LED_SignalSent();
 			//if (call TimeStamp.isValid(msg) == TRUE)
 			//	local.timestamp = call TimeStamp.timestamp(msg);
 			}
 		else
 			LED_SignalError();
 		sendBusy = FALSE;
 	}

 	/* Call-back event when finished read the temp */
 	event void ReadTemp.readDone(error_t result, uint16_t data) {
 		if (result != SUCCESS) {
 			data = 0xffff;
 			LED_SignalError();
 		}
 		local.temp_reading = data;
 		/* Release temperature resource */
 		if (call TempControl.isOwner() == TRUE)
 			call TempControl.release();
 		//local.count = call Timer.getNow()-local.count;
 		/* Acquired light sensor resource */

 		call LightControl.request();
 	}
 	/* Access permitted for light resource */
 	event void LightControl.granted() {
 		/* Read light sensor */
 		if (call ReadPhoto.read() != SUCCESS)
 			LED_SignalError();
 	}

 	event void ReadPhoto.readDone(error_t result, uint16_t data) {
 		uint16_t val;
 		if (result != SUCCESS)
 			LED_SignalError();
 		local.light_reading = data;
 		/* Release the light resource */
 		if (call LightControl.isOwner() == TRUE)
 			call LightControl.release();
 		/* Get the parameters required by Collection Protocol */
 		call CtpInfo.getEtx(&val);
 		local.etx = val;
 		call CtpInfo.getParent(&val);
 		local.link_route_addr = val;
 		local.link_route_value = call LinkEstimator.getLinkQuality(local.link_route_addr);

 		post TempConvertionTask();
 	}	
 	task void TempConvertionTask() {
 		double temp;
 		uint16_t realTemp = 0;
 		double const a = 0.0013075f;
 		double const b = 0.000214381f;
 		double const c = 0.000000093f;
 		double const R1 = 10000.0f;
 		double const ADC_FS = 1023.0f;
 		double T = a;
 		double lnRthemistor = ADC_FS - local.temp_reading;
 		double lnRthemistorCubed;
 		lnRthemistor = R1 * lnRthemistor;
 		lnRthemistor /= local.temp_reading;
 		lnRthemistor = log(lnRthemistor);
 		lnRthemistorCubed = lnRthemistor * lnRthemistor * lnRthemistor;
 		temp = b * lnRthemistor;
 		T = T + temp;
 		temp = c * lnRthemistorCubed;
 		T = T + temp;
 		T = 1.0f/T;
 		T = T - 272.15f;
 		T = T * 10.0f;
 		realTemp = (uint16_t) round(T);
 		local.temp_reading = realTemp;	

 		/* Reading mic value */
 		post readMicTask();
 	}
 	
 	async event error_t MicSetting.toneDetected() { 
 		post readMicTask();
 		return SUCCESS;
 	}

 	task void readMicTask() {
 		call ReadMic.read();
 	}

 	event void ReadMic.readDone(error_t err, uint16_t data) {
 		
 		if (err == SUCCESS) {
 			local.sound_presence = data;
 		}
 		else {
 			LED_SignalError();
 		}
 	}


 	event void SerialSend.sendDone (message_t* msg, error_t error) {
 		uartBusy = FALSE;
 	}
 	event void LinkEstimator.evicted(am_addr_t addr) { }


 	/* Functions for reporting issues and status */
 	static void init_error() {
 		call Leds.led0On();
 		call Leds.led1On();
 		call Leds.led2On();
 		call Timer.stop();
 	}

 	static void LED_SignalError() { call Leds.led0Toggle();}
 	static void LED_SignalSent() { call Leds.led1Toggle();}
 	static void LED_SignalReceived() { call Leds.led2Toggle();}
























 }